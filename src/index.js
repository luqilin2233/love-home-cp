import React from 'react';
// import ReactDOM from 'react-dom/client'; 该方法不适用于低版本
import ReactDOM from 'react-dom';
import './index.css';
import App from "./views/app/index"
import {BrowserRouter} from "react-router-dom"

// // 引入store
// import store from "./reduxore.js"
ReactDOM.render(
       // 在最外层的inde.js中用BrowserRouter把东西包裹起来，可以
       // 不用再到组件里面去配置BrowserRouter了，一劳永逸
        <BrowserRouter>
            <App/>
        </BrowserRouter>,
        document.getElementById("root")
)

