import './index.css';

import {observer} from "mobx-react"
// 引入数据概况
import StateSurvey from '../statesurvey/StateSurvey';
function App() {

  return (
    <div className="App">
      <StateSurvey/>
    </div>
  );
}
export default observer(App);
