import React from 'react';
import { Layout, Menu, Breadcrumb} from 'antd';
import "./index.module.less"
import{RadarChartOutlined}from "@ant-design/icons"


import {
     
     Link} from "react-router-dom"
import RouterIndex from '../../router';
const { Header, Content, Sider } = Layout;


function Home(){
         let data={
                         user:[
                    {
                         userName:"123456",
                         userId:1,
                         role:"管理员"
                    }
               ],
               role:[
                    {
                         url:"/url1",
                         name:"内容1",
                         icon:"RadarChartOutlined"
                    },
                    {
                         url:"/url2",
                         name:"内容2",
                         icon:"RadarChartOutlined"
                    },
                    {
                         url:"/url3",
                         name:"内容3",
                         icon:"RadarChartOutlined"
                    }
               ]
          
         }
         let showss=data.role.map((item,index)=>{
              return <Menu.Item key={item.url}>
                   <Link to={item.url}>{item.name}</Link>
              </Menu.Item>
              
            
         })
         
         
    
          return (
               <div>
                    <RadarChartOutlined />
                   <Layout>
                    <Header className="header">
                         <div className="logo" />
                         <Menu
                         theme="dark"
                         mode="horizontal"
                         defaultSelectedKeys={['2']}
                         style={{ lineHeight: '64px' }}
                         >
                         <Menu.Item key="1">nav 1</Menu.Item>
                         <Menu.Item key="2">nav 2</Menu.Item>
                         <Menu.Item key="3">nav 3</Menu.Item>
                         </Menu>
                    </Header>
     <Layout>
          <Sider width={200} style={{ background: '#fff' }}>
          <Menu
              mode="inline"
              // defaultSelectedKeys={['1']}
              // defaultOpenKeys={['sub1']}
              style={{ height: '100%', borderRight: 0 }}
              >
                   {showss}
               </Menu>
          </Sider>
          <Layout style={{ padding: '0 24px 24px' }}>
          <Breadcrumb style={{ margin: '16px 0' }}>
               <Breadcrumb.Item>Home</Breadcrumb.Item>
               <Breadcrumb.Item>List</Breadcrumb.Item>
               <Breadcrumb.Item>App</Breadcrumb.Item>
          </Breadcrumb>
          <Content
               style={{
               background: '#fff',
               padding: 24,
               margin: 0,
               minHeight: 280,
               }}
          >
              <RouterIndex/>
          </Content>
          </Layout>
    </Layout>
  </Layout>,
               </div>
          );
     }


export default Home;