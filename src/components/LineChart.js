//折线图组件
import React, { Component } from 'react';
// import stateSurvey from '../store';
import {Card} from 'antd'
//按需导入
// import echarts from 'echarts/lib/echarts'
//导入折线图
import 'echarts/lib/chart/line'
// 引入提示框和标题组件
import 'echarts/lib/component/tooltip'
import 'echarts/lib/component/title'
import 'echarts/lib/component/legend'
import 'echarts/lib/component/markPoint'
import ReactEcharts from 'echarts-for-react'
import stateSurvey from '../store/stateSurvey';
//引入样式
// import './common.less'
class LineChart extends Component {
    
      render() {
          return (
               
              <Card.Grid style={{width:"100%"}}>
                  <ReactEcharts option={stateSurvey.getOption()}/>
              </Card.Grid>
          )
      }
  }


export default LineChart;
