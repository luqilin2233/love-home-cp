import React, { Component } from 'react';
import { DatePicker, Space } from 'antd';
import moment from "moment"
const { RangePicker } = DatePicker;
const thisPro=["请选择起始日期","请选择结束日期"]
class HtitleDate extends Component {
     onOpenChangeFun(data,daStr){
          console.log(data)
          console.log(daStr)
     }
     render() {
          return (
               <div>
                    {/* 顶部的标题 */}
                    <div className='topDivSty'>
                         <div className='titltDivSty'></div>
                         <h3 className='TopDivH3Sty'>数据概况</h3>
                    </div>
                    {/* 日期选择组件 */}
                    <Space direction="vertical" size={12} className="RangePickerSty">
                         <RangePicker showToday="true" allowClear defaultValue={[moment('2022-01-01'), moment('2022-05-01')]} placeholder={thisPro} onChange={this.onOpenChangeFun}/>
                    </Space>
               </div>
          );
     }
}

export default HtitleDate;