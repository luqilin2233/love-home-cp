import {makeAutoObservable} from "mobx"
class Store{
    // 数据概况中的表格
     constructor(){
          makeAutoObservable(this)
     }
     // 数据初始化
     isLogin=false;
     a=1;
     // 改变数据的方法
     changeLogin(){
          this.isLogin=!this.isLogin
     }
     addA(){
          this.a++
     }
     get b(){
          return this.a*2
     }
      getOption = ()=>{
          let option = {
              title: {
                  text: '数据概况',
                  x: 'center'
              },
              tooltip:{
                  trigger: 'axis'
              },
              legend: {
                  // orient: 'vertical',
                  top: 20,
                  right: 50,
                  data:["居民数量",'签约量','服务量']
              },
              xAxis: {
                  data: ['1/4','1/5','1/6','1/7','1/8','1/9','1/10']
              },
              yAxis: {
                  type: 'value'
              },
              series : [
                  {
                      name:'居民数量',
                      type:'line',
                      data:[10, 20, 30, 25, 40, 50, 30]
                  },
                  {
                      name:'签约量',
                      type:'line',
                      data:[8, 25, 29, 20, 30, 45, 25]
                  },
                  {
                      name:'服务量',
                      type:'line',
                      data:[10, 25, 12, 40, 25, 30, 40]
                  }
              ]
          }
          return option;
      }
}
let  stateSurvey=new Store()
export default stateSurvey;